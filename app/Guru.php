<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'guru';
    public $primarykey = 'id';
    public $incrementing = true;

    protected $fillable = [
    	'id', 'nip', 'nama', 'alamat'
    ];

    public function kelas()
    {
    	return $this->hasMany('App\Kelas','id');
    }

}
