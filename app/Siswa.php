<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
	protected $table = 'siswa';
    public $primaryKey = 'nis';
    public $incrementing = false;

    public $fillable = [
        'nis','nama','id_kelas','nilai'
    ];

    public function kelas(){

    	return $this->belongsTo('App\Kelas','id_kelas','id_kelas');

    }
}
