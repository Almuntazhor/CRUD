<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
	protected $table = 'kelas';
    public $primaryKey = 'id_kelas';
    public $incrementing = false;

    public $fillable = [
            'id_kelas','nama_kelas', 'nama_jurusan', 'id'
    ];

    public function siswa()
    {
        return $this->hasMany('App\Siswa','id_kelas');
    }

    public function guru()
    {
        return $this->belongsTo('App\Guru','id','id');
    }
}
