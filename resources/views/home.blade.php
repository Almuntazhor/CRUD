@extends('app')

@section('title')
    Home
@endsection

@section('content')
    <br><br>
    <div class="col-md-2">
    </div>
    <div class="col-md-4">
        <div class="panel panel-info text-center">
            <div class="panel-heading">
                <h3>Siswa</h3>
            </div>
            <ul class="list-group">
                <li class="list-group-item"><i class="fa fa-user fa-5x"></i></li>
                <li class="list-group-item"><a href="/siswa" class="btn btn-primary"><i class="fa fa-user"></i> DATA SISWA</a></li>
            </ul>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info text-center">
            <div class="panel-heading">
                <h3>Guru</h3>
            </div>
            <ul class="list-group">
                <li class="list-group-item"><i class="fa fa-user fa-5x"></i></li>
                <li class="list-group-item"><a href="/guru" class="btn btn-primary"><i class="fa fa-user"></i> DATA GURU</a></li>
            </ul>
        </div>
    </div>
    <div class="col-md-4">
    </div>
    <div class="col-md-4">
        <div class="panel panel-info text-center">
            <div class="panel-heading">
                <h3>Kelas</h3>
            </div>
            <ul class="list-group">
                <li class="list-group-item"><i class="fa fa-university fa-5x"></i></li>
                <li class="list-group-item"><a href="/kelas" class="btn btn-primary"><i class="fa fa-university"></i> DATA KELAS</a></li>
            </ul>
        </div>
    </div>
    <div class="col-md-2">
    </div>
@endsection
