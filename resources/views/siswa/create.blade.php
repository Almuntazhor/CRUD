@extends('app')

@section('title')
    New Siswa
@endsection

@section('content')
    <div class="panel panel-default">
    <div class="panel-body">
	    <h4><i class="fa fa-plus-square"></i> TAMBAH SISWA</h4>
	    <hr>
        <div class="row">
	    	<div class="col-md-3">
				<div class="list-group">
				  <a href="#" class="list-group-item active">
				    <i class="fa fa-cogs"></i> MENU SISWA
				  </a>
				  <a href="/siswa" class="list-group-item"><i class="fa fa-refresh"></i> Tampilkan Semua</a>
				  <a href="/" class="list-group-item"><i class="fa fa-home"></i> Home</a>
				</div>
	        </div>

            <div class="col-md-6">
		    	<div class="panel panel-default">
	  				<div class="panel-body">
						{!! Form::open(array('url' => '/siswa')) !!}
						<div class="form-group"> <!-- ID field !-->
							<label for="nis" class="control-label">Nis</label>
							<input type="text" class="form-control" id="" name="nis" placeholder="No iduk siswa">
						</div>

						<div class="form-group"> <!-- Email field !-->
							<label for="" class="control-label">ID Kelas</label>
							  <div>
							  	<select name="id_kelas" class="form-control select" required data-error="ID Kelas">
									<?php if (!empty($kelas)): ?>
									<?php foreach($kelas as $data): ?>
									<option value="<?php echo $data->id_kelas;?>"><?php echo $data->id_kelas;?></option>
									<?php endforeach; ?>
									<?php endif; ?>
							  	</select>
							  </div>
					   </div>

                        <div class="form-group"> <!-- Email field !-->
							<label for="" class="control-label">Nama Siswa</label>
							<input type="text" class="form-control" id="" name="nama" placeholder="Nama Siswa">
						</div>
                        
                        <div class="form-group"> <!-- ID field !-->
							<label for="nilai" class="control-label">Nilai</label>
							<input type="text" class="form-control" id="" name="nilai" placeholder="Nilai">
						</div>

						{!! Form::button('<i class="fa fa-plus-square"></i>'. ' Simpan', array('type' => 'submit', 'class' => 'btn btn-primary'))!!}
                        {!! Form::button('<i class="fa fa-times"></i>'. ' Reset', array('type' => 'reset', 'class' => 'btn btn-danger'))!!}

						{!! Form::close()!!}
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
@endsection
