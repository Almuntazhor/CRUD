@extends('app')
@section('title')
    Data Siswa
@endsection
@section('content')
    <div class="panel panel-default">
    <div class="panel-body">
        <h4><i class="fa fa-user"></i> DAFTAR SISWA</h4><hr>

        <div class=row>
            <div class="col-md-6">
                <a href="/siswa/create" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
            </div>
        </div><br>
        @if(count($siswa))
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover table-condensed tfix">
                    <thead align="center">
                       <tr>
                           <td><b>NIS</b></td>
                           <td><b>Nama Siswa</b></td>
                           <td><b>Kelas</b></td>
                           <td><b>Jurusan</b></td>
                           <td><b>Nama Guru</b></td>
                           <td><b>Nilai</b></td>
                           <td colspan="2"><b>MENU</b></td>
                       </tr>
                   </thead>
                   @foreach($siswa as $val)
                       <tr align="center">
                           <td>{{ $val->nis }}</td>
                           <td>{{ $val->nama }}</td>
                           <td>{{ $val->kelas->nama_kelas }}</td>
                           <td>{{ $val->kelas->nama_jurusan }}</td>
                           <td>{{ $val->kelas->guru->nama }}</td>
                           <td>{{ $val->nilai }}</td>
                           <td align="center" width="30px">
                               <a href="/siswa/{{$val->nis}}/edit" class="btn btn-warning btn-sm" role="button"><i class="fa fa-pencil-square"></i> Edit</a>
                           </td>
                           <td align="center" width="30px">
                               {!! Form::open(array(
                                    'route' => array('siswa.destroy', $val->nis),
                                    'method' => 'delete',
                                    'style' => 'display:inline')) !!}
                                    <button class='btn btn-sm btn-danger delete-btn' type='submit'>
                                        <i class='fa fa-times-circle'></i> Delete
                                    </button>
                                {!! Form::close() !!}

                           </td>
                       </tr>
                   @endforeach
              </table>
          </div>
        @else
            <div class="alert alert-warning">
                <i class="fa fa-exclamation-triangle"></i> Data Siswa tidak tersedia
            </div>
        @endif
    </div>
    </div>
@endsection
