@extends('app')

@section('title')
    Edit Siswa
@endsection

@section('content')
    <div class="panel panel-default">
    <div class="panel-body">
	    <h4><i class="fa fa-check-square"></i> EDIT SISWA</h4>
	    <hr>
        <div class="row">
	    	<div class="col-md-3">
				<div class="list-group">
				  <a href="#" class="list-group-item active">
				    <i class="fa fa-cogs"></i> MENU SISWA
				  </a>
				  <a href="/siswa" class="list-group-item"><i class="fa fa-refresh"></i> Tampilkan Semua</a>
				  <a href="/" class="list-group-item"><i class="fa fa-home"></i> Home</a>
				</div>
	        </div>

            <div class="col-md-6">
		    	<div class="panel panel-default">
	  				<div class="panel-body">
						{!! Form::model($siswa,['method'=>'PATCH','action'=>['SiswaController@update',$siswa->nis]]) !!}
						
						<div class="form-group"> <!-- ID field !-->
							<label for="nis" class="control-label">Nis</label>
							<input type="text" class="form-control" id="" name="nis" placeholder="No iduk siswa" value="<?php echo $siswa->nis; ?>">
						</div>

						<div class="form-group"> <!-- Email field !-->
							<label for="" class="control-label">ID Kelas</label>
							  <div>
							  	<select name="id_kelas" class="form-control select" required data-error="ID Kelas" value="<?php echo $siswa->id_kelas; ?>">
									<?php if (!empty($kelas)): ?>
									<?php foreach($kelas as $data): ?>
									<option <?php echo ($siswa->id_kelas == $data->id_kelas) ? 'selected="selected"' : '';?> value="<?php echo $data->id_kelas;?>"><?php echo $data->id_kelas;?></option>
									<?php endforeach; ?>
									<?php endif; ?>
							  	</select>
							  </div>
					   </div>

                        <div class="form-group"> <!-- Email field !-->
							<label for="" class="control-label">Nama Siswa</label>
							<input type="text" class="form-control" id="" name="nama" placeholder="Nama Siswa" value="<?php echo $siswa->nama; ?>">
						</div>
                        
                        <div class="form-group"> <!-- ID field !-->
							<label for="nilai" class="control-label">Nilai</label>
							<input type="text" class="form-control" id="" name="nilai" placeholder="Nilai" value="<?php echo $siswa->nilai; ?>">
						</div>


						{!! Form::button('<i class="fa fa-check-square"></i>'. ' Update', array('type' => 'submit', 'class' => 'btn btn-primary'))!!}
						{!! Form::close()!!}
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
@endsection
