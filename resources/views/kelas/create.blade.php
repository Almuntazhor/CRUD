@extends('app')

@section('title')
    New Kelas
@endsection

@section('content')
    <div class="panel panel-default">
    <div class="panel-body">
	    <h4><i class="fa fa-plus-square"></i> TAMBAH KELAS</h4>
	    <hr>
        <div class="row">
	    	<div class="col-md-3">
				<div class="list-group">
				  <a href="#" class="list-group-item active">
				    <i class="fa fa-cogs"></i> MENU KELAS
				  </a>
				  <a href="/kelas" class="list-group-item"><i class="fa fa-refresh"></i> Tampilkan Semua</a>
				  <a href="/" class="list-group-item"><i class="fa fa-home"></i> Home</a>
				</div>
	        </div>

            <div class="col-md-6">
		    	<div class="panel panel-default">
	  				<div class="panel-body">
						{!! Form::open(array('url' => '/kelas')) !!}
						<div class="form-group"> <!-- ID field !-->
							<label for="id_kelas" class="control-label">ID Kelas</label>
							<input type="text" class="form-control" id="" name="id_kelas" placeholder="ID Kelas">
						</div>

						<div class="form-group"> <!-- Email field !-->
							<label for="" class="control-label">Nama Guru</label>
							  <div>
							  	<select name="id" class="form-control select" required data-error="Nama Guru">
									<?php if (!empty($guru)): ?>
									<?php foreach($guru as $data): ?>
									<option value="<?php echo $data->id;?>"><?php echo $data->nama;?></option>
									<?php endforeach; ?>
									<?php endif; ?>
							  	</select>
							  </div>
					   </div>

						<div class="form-group"> <!-- ID field !-->
							<label for="nama_kelas" class="control-label">Nama Kelas</label>
							<input type="text" class="form-control" id="" name="nama_kelas" placeholder="Nama Kelas">
						</div>

						<div class="form-group"> <!-- ID field !-->
							<label for="nama_jurusan" class="control-label">Nama Jurusan</label>
							<input type="text" class="form-control" id="" name="nama_jurusan" placeholder="Nama Jurusan">
						</div>
						<div class="form-group"> <!-- Submit button !-->
							<button type="submit" class="btn btn-primary"><i class="fa fa-plus-square"></i>Save</button>
						
							<button type="reset" class="btn btn-danger"><i class="fa fa-times"></i>Reset</button>
						</div>

						{!! Form::close()!!}
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
@endsection
