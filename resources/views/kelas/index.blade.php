@extends('app')

@section('title')
    Data Kelas
@endsection

@section('content')
    <div class="panel panel-default">
    <div class="panel-body">
        <h4><i class="fa fa-university"></i> DAFTAR KELAS</h4><hr>

        <div class=row>
            <div class="col-md-6">
                <a href="/kelas/create" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
            </div>
        </div><br>
        @if($kls->count())
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover table-condensed tfix">
                    <thead align="center">
                       <tr>
                           <td><b>ID Kelas</b></td>
                           <td><b>Nama Kelas</b></td>
                           <td><b>Nama Jurusan</b></td>
                           <td colspan="2"><b>MENU</b></td>
                       </tr>
                   </thead>
                   @foreach($kls as $k)
                       <tr align="center">
                           <td>{{ $k->id_kelas }}</td>
                           <td>{{ $k->nama_kelas }}</td>
                           <td>{{ $k->nama_jurusan }}</td>
                           <td align="center" width="30px">
                               <a href="/kelas/{{$k->id_kelas}}/edit" class="btn btn-warning btn-sm" role="button"><i class="fa fa-pencil-square"></i> Edit</a>
                           </td>
                           <td align="center" width="30px">
                               {!! Form::open(array(
                                    'route' => array('kelas.destroy', $k->id_kelas),
                                    'method' => 'delete',
                                    'style' => 'display:inline')) !!}
                                    <button class='btn btn-sm btn-danger delete-btn' type='submit'>
                                        <i class='fa fa-times-circle'></i> Delete
                                    </button>
                                {!! Form::close() !!}

                           </td>
                       </tr>
                   @endforeach
              </table>
          </div>
        @else
            <div class="alert alert-warning">
                <i class="fa fa-exclamation-triangle"></i> Data Kelas tidak tersedia
            </div>
        @endif
    </div>
    </div>
@endsection
