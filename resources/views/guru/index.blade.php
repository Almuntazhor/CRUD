@extends('app')

@section('title')
    Data guru
@endsection

@section('content')
    <div class="panel panel-default">
    <div class="panel-body">
        <h4><i class="fa fa-user"></i> DAFTAR GURU</h4><hr>

        <div class=row>
            <div class="col-md-6">
                <a href="/guru/create" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
            </div>
        </div><br>
        @if($guru->count())
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover table-condensed tfix">
                    <thead align="center">
                       <tr>
                           <td><b>ID</b></td>
                           <td><b>Nip</b></td>
                           <td><b>Nama</b></td>
                           <td><b>Alamat</b></td>
                           <td colspan="2"><b>MENU</b></td>
                       </tr>
                   </thead>
                   @foreach($guru as $val)
                       <tr align="center">
                           <td>{{ $val->id }}</td>
                           <td>{{ $val->nip }}</td>
                           <td>{{ $val->nama }}</td>
                           <td>{{ $val->alamat }}</td>
                           <td align="center" width="30px">
                               <a href="/guru/{{$val->id}}/edit" class="btn btn-warning btn-sm" role="button"><i class="fa fa-pencil-square"></i> Edit</a>
                           </td>
                           <td align="center" width="30px">
                               {!! Form::open(array(
                                    'route' => array('guru.destroy', $val->id),
                                    'method' => 'delete',
                                    'style' => 'display:inline')) !!}
                                    <button class='btn btn-sm btn-danger delete-btn' type='submit'>
                                        <i class='fa fa-times-circle'></i> Delete
                                    </button>
                                {!! Form::close() !!}

                           </td>
                       </tr>
                   @endforeach
              </table>
          </div>
        @else
            <div class="alert alert-warning">
                <i class="fa fa-exclamation-triangle"></i> Data guru tidak tersedia
            </div>
        @endif
    </div>
    </div>
@endsection
