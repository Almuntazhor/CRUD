@extends('app')

@section('title')
    New GURU
@endsection

@section('content')
    <div class="panel panel-default">
    <div class="panel-body">
	    <h4><i class="fa fa-plus-square"></i> TAMBAH GURU</h4>
	    <hr>
        <div class="row">
	    	<div class="col-md-3">
				<div class="list-group">
				  <a href="#" class="list-group-item active">
				    <i class="fa fa-cogs"></i> MENU GURU
				  </a>
				  <a href="/guru" class="list-group-item"><i class="fa fa-refresh"></i> Tampilkan Semua</a>
				  <a href="/" class="list-group-item"><i class="fa fa-home"></i> Home</a>
				</div>
	        </div>

            <div class="col-md-6">
		    	<div class="panel panel-default">
	  				<div class="panel-body">
						{!! Form::open(array('url' => '/guru')) !!}
						<div class="form-group"> <!-- ID field !-->
							<label for="id" class="control-label">ID</label>
							<input type="text" class="form-control" id="" name="id" placeholder="ID Guru">
						</div>
						<div class="form-group"> <!-- ID field !-->
							<label for="nip" class="control-label">Nip</label>
							<input type="text" class="form-control" id="" name="nip" placeholder="Nomer induk pegawai">
						</div>
                        <div class="form-group"> <!-- ID field !-->
							<label for="nama" class="control-label">Nama</label>
							<input type="text" class="form-control" id="" name="nama" placeholder="Nama lengkap">
						</div>
						<div class="form-group"> <!-- ID field !-->
							<label for="alamat" class="control-label">Alamat</label>
							<input type="text" class="form-control" id="" name="alamat" placeholder="Alamat tinggal saat ini">
						</div>
						<div class="form-group"> <!-- Submit button !-->
							<button type="submit" class="btn btn-primary"><i class="fa fa-plus-square"></i>Save</button>
						
							<button type="reset" class="btn btn-danger"><i class="fa fa-times"></i>Reset</button>
						</div>
						{!! Form::close()!!}
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
@endsection
